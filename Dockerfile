# В качестве родительского образа будем использовать готовый образ https://hub.docker.com/_/ruby из Docker Hub
FROM ruby:2.5-slim

# Все следующие команды будут выполнятся от имени root пользователя
USER root

# Установливаем програмнное обеспечения необходимое для корректной работы приложения
ENV BUILD_PACKAGES build-essential libpq-dev libxml2-dev libxslt1-dev nodejs imagemagick
RUN apt-get update -qq && apt-get install -y $BUILD_PACKAGES

# В environment переменную `APP_HOME` запишем путь по которому будет находится приложение внутри Docker образа
ENV APP_HOME /home/www/spreedemo
WORKDIR $APP_HOME

# Добавляем в текущий wordkir Gemfile и Gemfile.lock и вызываем команду по установке gem зависимостей.
# При отсутствие изменений в Gemfile и Gemfile.lock при следующем выполение команды будет использоваться кэш
ADD Gemfile Gemfile.lock ./
RUN bundle check || bundle install

# Добавляем в текущий wordkir все содержание приложения
ADD . .

# Даем owner права www-data юзеру на необходимые директории
RUN mkdir /var/www && \
    chown -R www-data:www-data /var/www && \
    chown -R www-data:www-data "$APP_HOME/."

# Все следующие команды будут выполнятся от имени www-data пользователя
USER www-data

# Поместим в образ скомпилированные assets
RUN bundle exec rake assets:precompile

# Команды которую будут выполнены только перед запуском контейнера
ADD docker-entrypoint.sh ./
ENTRYPOINT [ "./docker-entrypoint.sh" ]

# Дефолтноя команда по запуску образа
CMD bundle exec puma -C config/puma.rb
